Rails.application.routes.draw do
  resources :opendays
  resources :venues
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'venues#index'
  
  # resources to get data each platform
  get "platform_a/venue", to: 'apis#get_venue_a'
  get "platform_b/venue", to: 'apis#get_venue_b'
  get "platform_c/venue", to: 'apis#get_venue_c'
  
  # resources to update data
  put "platform_a/venue", to: 'apis#update_venue_a'
  put "platform_b/venue", to: 'apis#update_venue_b'
  put "platform_c/venue", to: 'apis#update_venue_c'
  

end
