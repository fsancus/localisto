class CreateOpendays < ActiveRecord::Migration
  def change
    create_table :opendays do |t|
      t.integer :day
      t.string :hours
      t.belongs_to :venue, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
