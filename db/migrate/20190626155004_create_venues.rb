class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :name
      t.string :address1
      t.string :address2
      t.decimal :lat, precision: 10, scale: 6
      t.decimal :lng, precision: 10, scale: 6
      t.integer :category_a_id
      t.integer :category_b_id
      t.boolean :closed
      t.string :website
      t.string :phone_numbre

      t.timestamps null: false
    end
  end
end
