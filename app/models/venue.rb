class Venue < ActiveRecord::Base


  has_many :opendays, :dependent => :destroy
  
  DAY_OF_WEEKS = [
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun"
  ]
  
  
  # Hours formated to Platform A Requirements
  def get_hours_a
    timetable = Array.new
    self.opendays.each do |item|
      timetable << item.hours     
    end
    timetable.join("|")
  end
  
  # Join two lines of adress (If exists second one)
  def get_adress
    adress = self.address2.blank? ? self.address1 : self.address1+" "+self.address2 
  end
  
  # Hours formated to Platform B Requirements
  def get_hours_b
    timetable = Array.new
    self.opendays.each do |item|
      timetable << (DAY_OF_WEEKS[item.day]+":"+item.hours)
    end
    timetable.join("|")
  end
  
  # Hours formated to Platform C Requirements
  def get_hours_c
    timetable = Array.new
    self.opendays.each do |item|
      timetable << item.hours     
    end
    timetable.join(",")
  end
  
  #Split and save open days in common format.
  #If the format received is unknow, it will be ignored
  def set_hours(hours)
    if(hours.start_with?( 'M'))
      splitted_hours = hours.split("|")
      self.opendays.destroy_all
      splitted_hours.each.with_index do |item, index|
         new_openday = Openday.create(day:index, hours:item[4,12])
         self.opendays << new_openday
      end
    elsif(hours.include?(","))
      splitted_hours = hours.split(",")
      self.opendays.destroy_all
      splitted_hours.each.with_index do |item, index|
         new_openday = Openday.create(day:index, hours:item)
         self.opendays << new_openday
      end
    
    elsif(hours.include?("|") && !hours.start_with?( 'M'))
      splitted_hours = hours.split("|")
      self.opendays.destroy_all
      splitted_hours.each.with_index do |item, index|
         new_openday = Openday.create(day:index, hours:item)
         self.opendays << new_openday
      end
    
    end
  end
  
end
