json.extract! openday, :id, :day, :hours, :venue_id, :created_at, :updated_at
json.url openday_url(openday, format: :json)
