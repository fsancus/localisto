json.extract! venue, :id, :name, :address1, :address2, :lat, :lng, :category_id, :closed, :website, :phone_numbre, :created_at, :updated_at
json.url venue_url(venue, format: :json)
