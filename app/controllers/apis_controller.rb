class ApisController < ApplicationController
  
  # @param api_key
  # return venue collection with structure A
  def get_venue_a
    venues = Array.new
    
    # Format data
    Venue.all.each do |item|
      new_venue = Hash.new
      new_venue[:name] = item.name
      new_venue[:lat] = item.lat
      new_venue[:lng] = item.lng
      new_venue[:hours] = item.get_hours_a
      new_venue[:adress] = item.get_adress
      new_venue[:closed] = item.closed
      new_venue[:category_id] = item.category_a_id      
      venues << new_venue
    end
    
    respond_to do |format|
      format.json{render json: venues.as_json}
    end
  end
  
  # @param api_key
  # return venue collection with structure B
  def get_venue_b
    venues = Array.new
    
    # Format data
    Venue.all.each do |item|
      new_venue = Hash.new
      new_venue[:name] = item.name
      new_venue[:lat] = item.lat
      new_venue[:lng] = item.lng
      new_venue[:hours] = item.get_hours_b
      new_venue[:streetadress] = item.get_adress
      new_venue[:closed] = item.closed
      new_venue[:category_id] = item.category_b_id      
      venues << new_venue
    end
    
    respond_to do |format|
      format.json{render json: venues.as_json}
    end  
  end
  
  # @param api_key
  # return venue collection with structure C
  def get_venue_c
    venues = Array.new
    
    # Format data
    Venue.all.each do |item|
      new_venue = Hash.new
      new_venue[:name] = item.name
      new_venue[:lat] = item.lat
      new_venue[:lng] = item.lng
      new_venue[:website] = item.website
      new_venue[:phone_numbre] = item.phone_numbre
      new_venue[:hours] = item.get_hours_c
      new_venue[:adress_line_1] = item.address1
      new_venue[:adress_line_2] = item.address2
      new_venue[:closed] = item.closed  
      venues << new_venue
    end
    
    respond_to do |format|
      format.json{render json: venues.as_json}
    end 
  end
  
  # @param api_key
  # patch to update venue
  # return update status
  def update_venue_a
    name = params[:name]
    addess1 = params[:adress]
    lat = params[:lat]
    lng = params[:lng]
    category_a_id = params[:category_id]
    closed = params[:category_id]
    hours = params[:hours]
    
    # Check item if is not blank
    errors = Array.new
    if((999 < category_a_id < 1201) || category_a_id.blank?)
      errors << "No category found"
    end
    if(!lat.nan? || lat.blank?)
      errors << "Latitude is wrong"
    end
    if(!lng.nan? || lng.blank?)
      errors << "Longitude is wrong"
    end
    if(!!closed != closed || closed.blank?)
      errors << "Closed value is wrong"
    end
    if(name.blank?) 
      errors << "Name value is wrong"
    end
    if(addess1.blank?)
      errors << "Adress value is wrong"
    end
    
    if(errors.count > 0)
      respond_to do |format|
        format.json{render json: {"status"=>"KO", "messages"=> errors.join(",")}}
      end 
    else
      # if the venue is new, insert new one
      if(Venue.find(params[:id]).nil?)
        new_venue =  Venue.new
        new_venue.name = name
        new_venue.address1 = addess1
        new_venue.lat = lat
        new_venue.lat = lat
        new_venue.category_a_id = category_a_id
        new_venue.closed = closed
        if(!new_venue.save!)
          errors<<"Error during saving new venue"
        else
          new_venue.set_hours(hours)
        end
      # If venue exists, update params
      else
        venue = Venue.find(params[:id])
        if(!venue.update_attributes(:name => name, :address1 => address1,:lat => lat, :lng => lng, :category_a_id=> category_a_id, :closed=> closed))
          errors << "Error during updating venue"
        else
          venue.set_hours(hours)
        end
      end
    end
    
  end
  
  # @param api_key
  # patch to update venue
  # return update status
  def update_venue_b
    name = params[:name]
    addess1 = params[:streetadress]
    lat = params[:lat]
    lng = params[:lng]
    category_b_id = params[:category_id]
    closed = params[:category_id]
    hours = params[:hours]
    
    # Check item if is not blank
    errors = Array.new
    if((1999 < category_a_id < 2201) || category_b_id.blank?)
      errors << "No category found"
    end
    if(!lat.nan? || lat.blank?)
      errors << "Latitude is wrong"
    end
    if(!lng.nan? || lng.blank?)
      errors << "Longitude is wrong"
    end
    if(!!closed != closed || closed.blank?)
      errors << "Closed value is wrong"
    end
    if(name.blank?) 
      errors << "Name value is wrong"
    end
    if(addess1.blank?)
      errors << "Adress value is wrong"
    end
          new_venue.set_hours(hours)
    
    if(errors.count > 0)
      respond_to do |format|
        format.json{render json: {"status"=>"KO", "messages"=> errors.join(",")}}
      end 
    else
      # if the venue is new, insert new one
      if(Venue.find(params[:id]).nil?)
        new_venue =  Venue.new
        new_venue.name = name
        new_venue.address1 = addess1
        new_venue.lat = lat
        new_venue.lat = lat
        new_venue.category_b_id = category_a_id
        new_venue.closed = closed
        if(!new_venue.save!)
          errors<<"Error during saving new venue"
        else
          new_venue.set_hours(hours)
        end
      # If venue exists, update params
      else
        venue = Venue.find(params[:id])
        if(!venue.update_attributes(:name => name, :address1 => address1,:lat => lat, :lng => lng, :closed=> closed))
          errors << "Error during updating venue"
        else
          venue.set_hours(hours)
        end
      end
    end
    
  end
  
  # @param api_key
  # patch to update venue
  # return update status
  def update_venue_c
    name = params[:name]
    addess1 = params[:adress_line_1]
    addess2 = params[:adress_line_2]
    lat = params[:lat]
    lng = params[:lng]
    closed = params[:category_id]
    website = params[:website]
    phone_numbre = params[:phone_numbre]
    hours = params[:hours]
    
    # Check item if is not blank
    errors = Array.new
    if(!lat.nan? || lat.blank?)
      errors << "Latitude is wrong"
    end
    if(!lng.nan? || lng.blank?)
      errors << "Longitude is wrong"
    end
    if(!!closed != closed || closed.blank?)
      errors << "Closed value is wrong"
    end
    if(name.blank?) 
      errors << "Name value is wrong"
    end
    if(addess1.blank?)
      errors << "Adress value is wrong"
    end
    
    if(errors.count > 0)
      respond_to do |format|
        format.json{render json: {"status"=>"KO", "messages"=> errors.join(",")}}
      end 
    else
      # if the venue is new, insert new one
      if(Venue.find(params[:id]).nil?)
        new_venue =  Venue.new
        new_venue.name = name
        new_venue.address1 = addess1
        new_venue.address2 = addess2
        new_venue.lat = lat
        new_venue.lat = lat
        new_venue.closed = closed
        new_venue.website = website
        new_venue.phone_numbre = phone_numbre
        if(!new_venue.save!)
          errors<<"Error during saving new venue"
        else
          new_venue.set_hours(hours)
        end
      # If venue exists, update params
      else
        venue = Venue.find(params[:id])
        if(!venue.update_attributes(:name => name, :address1 => address1,:lat => lat, :lng => lng, :website=> website, :phone_numbre=>phone_numbre, :closed=> closed))
          errors << "Error during updating venue"
        else
          venue.set_hours(hours)
        end
      end
    end
    
  end
  
end