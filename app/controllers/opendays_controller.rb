class OpendaysController < ApplicationController
  before_action :set_openday, only: [:show, :edit, :update, :destroy]

  # GET /opendays
  # GET /opendays.json
  def index
    @opendays = Openday.all
  end

  # GET /opendays/1
  # GET /opendays/1.json
  def show
  end

  # GET /opendays/new
  def new
    @openday = Openday.new
  end

  # GET /opendays/1/edit
  def edit
  end

  # POST /opendays
  # POST /opendays.json
  def create
    @openday = Openday.new(openday_params)

    respond_to do |format|
      if @openday.save
        format.html { redirect_to @openday, notice: 'Openday was successfully created.' }
        format.json { render :show, status: :created, location: @openday }
      else
        format.html { render :new }
        format.json { render json: @openday.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /opendays/1
  # PATCH/PUT /opendays/1.json
  def update
    respond_to do |format|
      if @openday.update(openday_params)
        format.html { redirect_to @openday, notice: 'Openday was successfully updated.' }
        format.json { render :show, status: :ok, location: @openday }
      else
        format.html { render :edit }
        format.json { render json: @openday.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /opendays/1
  # DELETE /opendays/1.json
  def destroy
    @openday.destroy
    respond_to do |format|
      format.html { redirect_to opendays_url, notice: 'Openday was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_openday
      @openday = Openday.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def openday_params
      params.require(:openday).permit(:day, :hours, :venue_id)
    end
end
