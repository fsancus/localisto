require 'test_helper'

class OpendaysControllerTest < ActionController::TestCase
  setup do
    @openday = opendays(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:opendays)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create openday" do
    assert_difference('Openday.count') do
      post :create, openday: { day: @openday.day, hours: @openday.hours, venue_id: @openday.venue_id }
    end

    assert_redirected_to openday_path(assigns(:openday))
  end

  test "should show openday" do
    get :show, id: @openday
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @openday
    assert_response :success
  end

  test "should update openday" do
    patch :update, id: @openday, openday: { day: @openday.day, hours: @openday.hours, venue_id: @openday.venue_id }
    assert_redirected_to openday_path(assigns(:openday))
  end

  test "should destroy openday" do
    assert_difference('Openday.count', -1) do
      delete :destroy, id: @openday
    end

    assert_redirected_to opendays_path
  end
end
